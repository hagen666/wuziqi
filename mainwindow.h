#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QMenu>
#include <QMenuBar>
#include <QMouseEvent>
#include <QDebug>
#include <QTimer>
#include "gamemodel.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
protected:
    void paintEvent(QPaintEvent* event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent* event);
private:
    Ui::MainWindow *ui;
    GameModel* model;
    GameType gameType;
    void initPVPGame();
    void initPVEGame();
    void chessOneByPerson();
    void chessOneByAI();

    //void chessOneByStorage();
    // AI 帮助下棋
    void chessOneByHelper();
    // AI 提示落子
    void LightOneByHelper();
    void light();
    void updateStatusTips();
    int deep;

    //悔棋
    void chessBack();
    int backFlag;


};

#endif // MAINWINDOW_H
