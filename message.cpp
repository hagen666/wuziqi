#include "message.h"

void Message::toJSON(QJsonObject &json)
{

}

void Message::fromJSON(QJsonObject &json)
{

}

QJsonObject getJsonObjectFromString(const QString jsonString){
    QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonString.toLocal8Bit().data());
    QJsonObject jsonObject = jsonDocument.object();
    return jsonObject;
}
// QJson >> QString
QString getStringFromJsonObject(const QJsonObject& jsonObject){
    return QString(QJsonDocument(jsonObject).toJson());
}

RoomMessage::RoomMessage(){this->msgType=roomMsgType;}
ChessMessage::ChessMessage(){this->msgType = chessMsgType;}
ChatMessage::ChatMessage(){this->msgType = chatMsgType;}
WinMessage::WinMessage(){this->msgType = winMsgType;}
Message* Message::readMessage(QTcpSocket* socket)
{
    //while(socket->bytesAvailable()<4);


}
void WinMessage::fromJSON(QJsonObject &json)
{

}
void WinMessage::toJSON(QJsonObject &json)
{

}
void ChatMessage::fromJSON(QJsonObject &json)
{

}
void ChatMessage::toJSON(QJsonObject &json)
{
    json.insert("from",from);
    json.insert("toWhom",toWhom);
    json.insert("message",message);
}
void ChessMessage::toJSON(QJsonObject &json)
{
    json.insert("row",row);
    json.insert("col",col);
    json.insert("value",value);
    json.insert("backAllowd",backAllowd);
    json.insert("msgType",msgType);
}
void ChessMessage::fromJSON(QJsonObject &json)
{
    row = json["row"].toInt();
    col = json["col"].toInt();
    value = json["value"].toInt();
    backAllowd = json["backAllowd"].toBool();
}
void RoomMessage::toJSON(QJsonObject &json)
{
    json.insert("msgType",msgType);
    json.insert("roomId",roomId);
    json.insert("fifc",fifc);
    QJsonArray arr;
    for(int i=0;i<name.size();i++)
    {
        arr.append(name[i]);
    }
    json.insert("name",arr);
}
void RoomMessage::fromJSON(QJsonObject &json)
{
    roomId = json["roomId"].toInt();
    fifc = json["fifc"].toBool();
    QJsonArray arr = json["name"].toArray();
    name.clear();
    for(int i = 0;i<arr.size();i++)
    {
        name.push_back(arr[i].toString());
    }
}

void writeMessage(QTcpSocket* socket,Message* msg)
{
    char data[4];
    memset(data,0,4);
    memcpy(data,&msg->msgType,4);
    socket->write(data,4);
    QJsonObject obj;
    msg->toJSON(obj);
    QString str = getStringFromJsonObject(obj);
    QByteArray arr = str.toUtf8();
    int length = arr.length();
    memcpy(data,&length,4);
    socket->write(data,4);
    socket->write(arr);
}
void releaseMessage(Message*& msg)
{
    delete msg;
    msg = 0;
}
