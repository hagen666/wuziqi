#ifndef DRAWBOARD_H
#define DRAWBOARD_H

#include <QObject>
#include <QLabel>

class DrawBoard : public QLabel
{
    Q_OBJECT
public:
    DrawBoard(QWidget *parent = 0);
    ~DrawBoard();
};

#endif // DRAWBOARD_H
