#include "gamemodel.h"
#include <utility>
#include <stdlib.h>
#include <time.h>
#include "common.h"
#include <QDebug>
static bool inRange(int pos)
{
    if(pos>0&&pos<kBoardSizeNum)return true;
    return false;
}
GameModel::GameModel()
{
    testFlag = false;
}
void GameModel::actionByPerson(int row, int col)
{
    updateGameMap(row, col);
    //pointHistory.push_back(std::make_pair(row, col));
}
int GameModel::evaluate(int color,int row,int col,double* res,int* side)
{
    int num[6];
    int numside[6];
    int score = 0;
    int checkRow = 0, checkCol=0;
    int offsetRow[] = {0,1,1,1};
    int offsetCol[] = {1,0,1,-1};
    for(int i=0;i<7;i++){
        num[i]=0;
        numside[i]=0;
        if(i<4)
        {res[i]=0;side[i]=0;}
    }
    if(color!=0&&gameMap[row][col]==0)
    {
        for(int i=0;i<4;i++)
        {
            checkRow = row;
            checkCol = col;
            bool leftIsEmpty = false;
            bool rightIsEmpty = false;
            do
            {
                checkRow-=offsetRow[i];
                checkCol-=offsetCol[i];
            }while(inRange(checkRow)&&inRange(checkCol)&&gameMap[checkRow][checkCol]==color);
            if(inRange(checkRow)&&inRange(checkCol)&&gameMap[checkRow][checkCol]==0)leftIsEmpty=true;
            int k = 0;
            checkRow+=offsetRow[i];
            checkCol+=offsetCol[i];
            do
            {
                k++;
                checkRow+=offsetRow[i];
                checkCol+=offsetCol[i];
            }while(inRange(checkRow)&&inRange(checkCol)&&gameMap[checkRow][checkCol]==color);
            if(inRange(checkRow)&&inRange(checkCol)&&gameMap[checkRow][checkCol]==0)rightIsEmpty=true;
            res[i]=k;
            if(leftIsEmpty)side[i]++;
            if(rightIsEmpty)side[i]++;
            num[k]++;
            if(side[i]>1)numside[k]++;
        }
    }
    if(num[2]>0)
    {
        score+=10*num[2];
        if(numside[2]>0)score+=10*numside[2];
    }
    if(num[3]>0)
    {
        score+=30+10*(num[3]-1);
        if(numside[3]>0)score+=20+10*(numside[3]-1);
    }
    if(num[4]>0)
    {
        score+=40+10*(num[4]-1);
        if(numside[4]>0)score+=20+10*(numside[4]-1);
    }
    if(num[5]>0)score+=1000*num[5];
   // if(num[6]>0)score+=1000*num[5];
    return score;

}
void GameModel::actionByAI2(int &clickRow, int &clickCol)
{
    //int color = currentIsBlack?1:-1;
    double res[] = {0.0,0.0,0.0,0.0};
    int side[]={0,0,0,0};
    double maxScoreWhite = 0;
    std::vector<std::pair<int, int>> maxPointsWhite;
    double maxScoreBlack = 0;
    std::vector<std::pair<int, int>> maxPointsBlack;
    for(int p=1;p<kBoardSizeNum;p++)
        for(int q=1;q<kBoardSizeNum;q++)
        {
            int row = p;
            int col = q;
            if (gameMap[row][col] == 0)
            {
                scoreMapBlack[row][col]=evaluate(1,row,col,res,side);
                scoreMapWhite[row][col]=evaluate(-1,row,col,res,side);
                if (scoreMapWhite[row][col] > maxScoreWhite)          // 找最大的数和坐标
                {
                    maxPointsWhite.clear();
                    maxScoreWhite = scoreMapWhite[row][col];
                    maxPointsWhite.push_back(std::make_pair(row, col));
                }
                else if (scoreMapWhite[row][col] == maxScoreWhite)     // 如果有多个最大的数，都存起来
                    maxPointsWhite.push_back(std::make_pair(row, col));
                if (scoreMapBlack[row][col] > maxScoreBlack)          // 找最大的数和坐标
                {
                    maxPointsBlack.clear();
                    maxScoreBlack = scoreMapBlack[row][col];
                    maxPointsBlack.push_back(std::make_pair(row, col));
                }
                else if (scoreMapBlack[row][col] == maxScoreBlack)     // 如果有多个最大的数，都存起来
                    maxPointsBlack.push_back(std::make_pair(row, col));
            }
        }
    srand((unsigned)time(0));
    //qDebug()<<"max score black"<<maxScoreBlack<<" max score white "<<maxScoreWhite;
    if(maxScoreBlack<maxScoreWhite)
    {
        int index = rand() % maxPointsWhite.size();
        std::pair<int, int> pointPair = maxPointsWhite.at(index);
        clickRow = pointPair.first; // 记录落子点
        clickCol = pointPair.second;
        updateGameMap(clickRow, clickCol);
        //pointHistory.push_back(std::make_pair(clickRow, clickCol));
     }else
    {
        int index = rand() % maxPointsBlack.size();
        std::pair<int, int> pointPair = maxPointsBlack.at(index);
        clickRow = pointPair.first; // 记录落子点
        clickCol = pointPair.second;
        updateGameMap(clickRow, clickCol);
    }

}
void actionBack(int& row,int& col)
{

}
void GameModel::actionByAI(int &clickRow, int &clickCol)
{
    // 计算评分
    //calculateScore();

    // 从评分中找出最大分数的位置
    double maxScoreWhite = 0;
    std::vector<std::pair<int, int>> maxPointsWhite;
    double maxScoreBlack = 0;
    std::vector<std::pair<int, int>> maxPointsBlack;
    for (int row = 1; row < kBoardSizeNum; row++)
        for (int col = 1; col < kBoardSizeNum; col++)
        {
            // 前提是这个坐标是空的
            if (gameMap[row][col] == 0)
            {
                if (scoreMapWhite[row][col] > maxScoreWhite)          // 找最大的数和坐标
                {
                    maxPointsWhite.clear();
                    maxScoreWhite = scoreMapWhite[row][col];
                    maxPointsWhite.push_back(std::make_pair(row, col));
                }
                else if (scoreMapWhite[row][col] == maxScoreWhite)     // 如果有多个最大的数，都存起来
                    maxPointsWhite.push_back(std::make_pair(row, col));
                if (scoreMapBlack[row][col] > maxScoreBlack)          // 找最大的数和坐标
                {
                    maxPointsBlack.clear();
                    maxScoreBlack = scoreMapBlack[row][col];
                    maxPointsBlack.push_back(std::make_pair(row, col));
                }
                else if (scoreMapBlack[row][col] == maxScoreBlack)     // 如果有多个最大的数，都存起来
                    maxPointsBlack.push_back(std::make_pair(row, col));
            }
        }

    // 随机落子，如果有多个点的话
    srand((unsigned)time(0));
    qDebug()<<"max score black"<<maxScoreBlack<<" max score white "<<maxScoreWhite;
    if((currentIsBlack&&maxScoreBlack+1<maxScoreWhite)||(!currentIsBlack&&(maxScoreWhite+1>maxScoreBlack)))
    {
        int index = rand() % maxPointsWhite.size();
        std::pair<int, int> pointPair = maxPointsWhite.at(index);
        clickRow = pointPair.first; // 记录落子点
        clickCol = pointPair.second;
        updateGameMap(clickRow, clickCol);
        //pointHistory.push_back(std::make_pair(clickRow, clickCol));
     }else
    {
        int index = rand() % maxPointsBlack.size();
        std::pair<int, int> pointPair = maxPointsBlack.at(index);
        clickRow = pointPair.first; // 记录落子点
        clickCol = pointPair.second;
        updateGameMap(clickRow, clickCol);
        //pointHistory.push_back(std::make_pair(clickRow, clickCol));
    }
}
void GameModel::setEmptyPosition(int& row, int& col)
{
    std::vector<std::pair<int, int> >::iterator iter;
    iter = pointHistory.end();
    iter--;
    row = iter->first;
    col = iter->second;
    gameMap[row][col] = 0;
    pointHistory.erase(iter);
    if(testFlag)return ;
    // 换手
    if(gameType==BOT||gameType==NETWORK)playerFlag = !playerFlag;
    currentIsBlack = !currentIsBlack;
}
void GameModel::updateGameMap(int row, int col)
{
    if (currentIsBlack)
        gameMap[row][col] = 1;
    else
        gameMap[row][col] = -1;
    if(testFlag)return ;

    /*
    int offsetRow[] = {0,1,1,1};
    int offsetCol[] = {1,0,1,-1};
    for(int i=0;i<4;i++)
    {
        int checkRow = row;
        int checkCol = col;
        for(int j = 4;j>0;j--)
        {
            checkRow-=offsetRow[i];
            checkCol-=offsetCol[i];
            if(!inRange(checkRow)||!inRange(checkCol))break;
            if(currentIsBlack)
            {
                scoreMapBlack[checkRow][checkCol]+=(1+j*0.1);
                scoreMapWhite[checkRow][checkCol]-=(1+j*0.1);
            }else
            {
                scoreMapWhite[checkRow][checkCol]+=(1+j*0.1);
                scoreMapBlack[checkRow][checkCol]-=(1+j*0.1);
            }
        }
        checkRow=row;
        checkCol=col;
        for(int j = 4;j>0;j--)
        {
            checkRow+=offsetRow[i];
            checkCol+=offsetCol[i];
            if(!inRange(checkRow)||!inRange(checkCol))break;
            if(currentIsBlack)
            {
                scoreMapBlack[checkRow][checkCol]+=(1+j*0.1);
                scoreMapWhite[checkRow][checkCol]-=(1+j*0.1);
            }else
            {
                scoreMapWhite[checkRow][checkCol]+=(1+j*0.1);
                scoreMapBlack[checkRow][checkCol]-=(1+j*0.1);
            }
        }
    }*/
    /*
    qDebug()<<"black score map:";
    for(int i=1;i<kBoardSizeNum;i++)
    {
        QString str;
        QString apd;
        for(int j=1;j<kBoardSizeNum;j++)
        {
            apd.sprintf("%.1f",scoreMapBlack[i][j]);
            if(scoreMapBlack[i][j]>=0)str+="+";
            str+=apd;
            str=str.append(' ');
        }
        qDebug()<<str;
    }
    qDebug()<<"white score map:";
    for(int i=1;i<kBoardSizeNum;i++)
    {
        QString str;
        QString apd;
        for(int j=1;j<kBoardSizeNum;j++)
        {
            apd.sprintf("%.1f",scoreMapWhite[i][j]);
            if(scoreMapWhite[i][j]>=0)str+="+";
            str+=apd;
            str=str.append(' ');
        }
        qDebug()<<str;
    }*/
    // 换手
    pointHistory.push_back(std::make_pair(row,col));
    if(gameType==BOT||gameType==NETWORK)playerFlag = !playerFlag;
    currentIsBlack = !currentIsBlack;
}


bool GameModel::isDeadGame()
{
    // 所有空格全部填满
    for (int i = 1; i < kBoardSizeNum; i++)
        for (int j = 1; j < kBoardSizeNum; j++)
        {
            if (!(gameMap[i][j] == 1 || gameMap[i][j] == -1))
                return false;
        }
    return true;
}
bool GameModel::isWin(int row,int col)
{
    int color = gameMap[row][col];
    int checkRow = row;
    int checkCol = col;
    int offsetRow[] = {0,1,1,1};
    int offsetCol[] = {1,0,1,-1};
    int srcRow,srcCol,tarRow,tarCol;
    if(color!=0)
    {
        //
        for(int i=0;i<4;i++)
        {
            checkRow = row;
            checkCol = col;
            while(inRange(checkRow)&&inRange(checkCol)&&gameMap[checkRow][checkCol]==color)
            {
                checkRow-=offsetRow[i];
                checkCol-=offsetCol[i];
            }
            int k = 0;
            checkRow+=offsetRow[i];
            checkCol+=offsetCol[i];
            srcRow=checkRow;
            srcCol=checkCol;
            //k++;
            do
            {
                k++;
                if(k>=5){
                    tarCol = checkCol;
                    tarRow = checkRow;
                   // qDebug()<<"i="<<i<<"suc("<<srcRow<<","<<srcCol<<")"<<" ("<<tarRow<<","<<tarCol<<") k="<<k;
                    return true;
                }
                checkRow+=offsetRow[i];
                checkCol+=offsetCol[i];
            }while(inRange(checkRow)&&inRange(checkCol)&&gameMap[checkRow][checkCol]==color);
            tarCol = checkCol-offsetCol[i];
            tarRow = checkRow-offsetRow[i];
           // qDebug()<<"i="<<i<<" ("<<srcRow<<","<<srcCol<<")"<<" ("<<tarRow<<","<<tarCol<<") k="<<k;
        }
    }
    return false;
}
void GameModel::startGame(GameType type)
{
    gameType = type;
    gameMap.clear();
    for(int i = 0; i <kBoardSizeNum;i++)
    {
        std::vector<int> lineBoard;
        for(int j=0;j<kBoardSizeNum;j++)
        {
            lineBoard.push_back(0);
        }
        gameMap.push_back(lineBoard);
    }
    scoreMapWhite.clear();
    scoreMapBlack.clear();
    for(int i = 0; i <kBoardSizeNum;i++)
    {
        std::vector<double> lineBoard;
        for(int j=0;j<kBoardSizeNum;j++)
        {
            lineBoard.push_back(0.0);
        }
        scoreMapWhite.push_back(lineBoard);
    }
    for(int i = 0; i <kBoardSizeNum;i++)
    {
        std::vector<double> lineBoard;
        for(int j=0;j<kBoardSizeNum;j++)
        {
            lineBoard.push_back(0.0);
        }
        scoreMapBlack.push_back(lineBoard);
    }
    playerFlag = true;
    currentIsBlack = true;
}
