#ifndef COMMON_H
#define COMMON_H
#define AIDELAY 500
const int kBoardMarginL = 20; // 棋盘边缘空隙
const int kBoardMarginH = 50; // 棋盘边缘空隙
const int kRadius = 13; // 棋子半径
const int kMarkSize = 6; // 落子标记边长
const int kBlockSize = 35; // 格子的大小
const int kPosDelta = 20; // 鼠标点击的模糊距离上限
static bool playerFlag = false;
static int clickPosRow = -1;
static int clickPosCol = -1;
static int clickColor = 0;

static int sRow = -1;
static int sCol = -1;
static int sColor = 0;
const int kBoardSizeNum = 15;

#endif // COMMON_H
