#ifndef GAMEMODEL_H
#define GAMEMODEL_H
#include <vector>

/** 需要修改 GameType里包含很多信息：
 * 包括是我方先手还是对方先手、
 * 是网络还是个人还是机器
 * 网络的话应该包括IP信息，房间信息，是否允许使用提示等
 */
enum GameType
{PERSON,BOT,NETWORK};
enum GameStatus
{PLAYING,WAITING,DEAD,WIN};

class GameModel
{
public:
    GameModel();
    std::vector<std::vector<int>> gameMap;
    std::vector<std::pair<int, int>> pointHistory;
    std::vector<std::vector<double>> scoreMapBlack;
    std::vector<std::vector<double>> scoreMapWhite;
    bool playerFlag;
    bool currentIsBlack;
    GameType gameType;
    GameStatus gameStatus;
    bool testFlag;
    void startGame(GameType type);
    void genScore();
    void actionByPerson(int row,int col);
    void actionByAI(int &clickRow,int &clickCol);
    void actionByAI2(int &clickRow,int &clickCol);
    //void actionBack(int& row,int& col);
    void updateGameMap(int row,int col);
    void setEmptyPosition(int &row, int &col);
    bool isWin(int row,int col);
    bool isDeadGame();
    int evaluate(int color,int row,int col,double* res,int* side);
};

#endif // GAMEMODEL_H
