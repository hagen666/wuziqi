#ifndef MESSAGE_H
#define MESSAGE_H
#include <vector>
#include <QTcpSocket>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
const int roomMsgType = 0x97;
const int chessMsgType = 0x98;
const int chatMsgType = 0x99;
const int winMsgType = 0x96;

class Message:public QObject
{
    Q_OBJECT
public:
   // NMessage();
    int msgType;
    int length;

    static Message* readMessage(QTcpSocket*);
    static void writeMessage(QTcpSocket*,Message*);
    static void releaseMessage(Message*&);
    virtual void toJSON(QJsonObject& json);
    virtual void fromJSON(QJsonObject& json);
};

class RoomMessage: public Message
{
    Q_OBJECT
public:
    RoomMessage();
    int roomId;
    std::vector<QString> name;
    bool fifc;
    void toJSON(QJsonObject& json);
    void fromJSON(QJsonObject& json);
};

class ChessMessage: public Message
{
    Q_OBJECT
public:
    ChessMessage();
    int row;
    int col;
    int value;
    bool backAllowd;
    void toJSON(QJsonObject& json);
    void fromJSON(QJsonObject& json);
};

class ChatMessage:public Message
{
    Q_OBJECT
public:
    ChatMessage();
    QString from;
    QString toWhom;
    QString message;
    void toJSON(QJsonObject& json);
    void fromJSON(QJsonObject& json);
};
class WinMessage: public Message
{
    Q_OBJECT
public:
    WinMessage();
    int winColor;
    void toJSON(QJsonObject& json);
    void fromJSON(QJsonObject& json);
};


#endif // MESSAGE_H
