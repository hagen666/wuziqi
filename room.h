#ifndef ROOM_H
#define ROOM_H
#include <vector>
#include <QTcpSocket>
#include <map>
class Room;
class User
{
public:
    User();
    QTcpSocket* socket;
    QString name;
    Room* room;
};
class Room
{
public:
    Room();
    std::vector<std::vector<int>> gameMap;
    std::vector<std::pair<int, int>> pointHistory;
    std::vector<User*> userList;
    bool fifc;
private:

};



#endif // ROOM_H
