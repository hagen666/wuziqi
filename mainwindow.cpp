#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "drawboard.h"
#include "common.h"
#include <QMessageBox>
#include <QDebug>
#include <QThread>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
  ,  ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->label->hide();
    setMouseTracking(true);
    centralWidget()->setMouseTracking(true);
    this->update();
    model = new GameModel();
    initPVPGame();
    connect(ui->aiButton,&QPushButton::clicked,this,&MainWindow::chessOneByHelper);
    connect(ui->aihelper,&QPushButton::clicked,this,&MainWindow::LightOneByHelper);
    connect(ui->pveAction,&QAction::triggered,this,&MainWindow::initPVEGame);
    connect(ui->pvpAction,&QAction::triggered,this,&MainWindow::initPVPGame);
    connect(ui->backButton,&QPushButton::clicked,this,&MainWindow::chessBack);
   // ui->label->setStyleSheet("border-image:url(:/images/wl2)");
    backFlag = 0;
    statusBar();

}
void MainWindow::paintEvent(QPaintEvent* event){
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    //int kBoardMargin =5;
    //int kBlockSize= 40;
    int height = 2*kBoardMarginH+kBlockSize*kBoardSizeNum;
    int width = 2*kBoardMarginL+kBlockSize*kBoardSizeNum;
    for (int i = 0; i < kBoardSizeNum + 1; i++)
    {
        painter.drawLine(kBoardMarginL + kBlockSize * i, kBoardMarginH, kBoardMarginL + kBlockSize * i, height - kBoardMarginH);
        painter.drawLine(kBoardMarginL, kBoardMarginH + kBlockSize * i, width - kBoardMarginL, kBoardMarginH + kBlockSize * i);
    }

    QBrush brush;
    brush.setStyle(Qt::SolidPattern);

    if (clickPosRow > 0 && clickPosRow < kBoardSizeNum &&
        clickPosCol > 0 && clickPosCol < kBoardSizeNum
            //&&game->gameMapVec[clickPosRow][clickPosCol] == 0
            )
    {
        if (!model->currentIsBlack)
            brush.setColor(Qt::white);
        else
            brush.setColor(Qt::black);
        painter.setBrush(brush);
       // qDebug()<<"2x "<<clickPosCol<<"2y "<<clickPosRow<<"\n";
        painter.drawRect(kBoardMarginL + kBlockSize * clickPosCol - kMarkSize / 2, kBoardMarginH + kBlockSize * clickPosRow - kMarkSize / 2, kMarkSize, kMarkSize);
    }
    for (int i = 0; i < kBoardSizeNum; i++)
        for (int j = 0; j < kBoardSizeNum; j++)
        {
            if (model->gameMap[i][j] == 1)
            {
                brush.setColor(Qt::black);
                painter.setBrush(brush);
                painter.drawEllipse(kBoardMarginL + kBlockSize * j - kRadius, kBoardMarginH + kBlockSize * i - kRadius, kRadius * 2, kRadius * 2);
            }
            else if (model->gameMap[i][j] == -1)
            {
                brush.setColor(Qt::white);
                painter.setBrush(brush);
                painter.drawEllipse(kBoardMarginL + kBlockSize * j - kRadius, kBoardMarginH + kBlockSize * i - kRadius, kRadius * 2, kRadius * 2);
            }
        }

    if (clickPosRow > 0 && clickPosRow < kBoardSizeNum &&
        clickPosCol > 0 && clickPosCol < kBoardSizeNum &&
        (model->gameMap[clickPosRow][clickPosCol] == 1 ||
            model->gameMap[clickPosRow][clickPosCol] == -1))
    {
        if (model->isWin(clickPosRow, clickPosCol) && model->gameStatus == PLAYING)
        {
            qDebug() << "win";
            model->gameStatus = WIN;
            //QSound::play(WIN_SOUND);
            QString str;
            if (model->gameMap[clickPosRow][clickPosCol] == 1)
                str = "black player";
            else if (model->gameMap[clickPosRow][clickPosCol] == -1)
                str = "white player";
            QMessageBox::StandardButton btnValue = QMessageBox::information(this, "congratulations", str + "win!");

            // 重置游戏状态，否则容易死循环
            if (btnValue == QMessageBox::Ok)
            {
                model->startGame(gameType);
                model->gameStatus = PLAYING;
            }
        }
    }


    // 判断死局
    if (model->isDeadGame())
    {
        //QSound::play(LOSE_SOUND);
        QMessageBox::StandardButton btnValue = QMessageBox::information(this, "oops", "dead game!");
        if (btnValue == QMessageBox::Ok)
        {
            model->startGame(gameType);
            model->gameStatus = PLAYING;
        }

    }
     updateStatusTips();
}
void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    int x = event->x();
    int y = event->y();
    int height = 2*kBoardMarginH+kBlockSize*kBoardSizeNum;
    int width = 2*kBoardMarginL+kBlockSize*kBoardSizeNum;
    if(x>=kBoardMarginL+kBlockSize/2&&x<width-kBoardMarginL&&
            y>= kBoardMarginH+kBlockSize/2&&y<height-kBoardMarginH)
    {
        int col = (x-kBoardMarginL+kBlockSize/2)/kBlockSize;
        int row = (y-kBoardMarginH+kBlockSize/2)/kBlockSize;

        int leftX = kBlockSize*col+kBoardMarginL;
        int leftY = kBlockSize*row+kBoardMarginH;
        clickPosCol = col;
        clickPosRow = row;
        //qDebug()<<"x "<<clickPosCol<<"y "<<clickPosRow<<"\n";

    }

 update();
 //updateStatusTips();

}
void MainWindow::updateStatusTips()
{
    QString str;
    str+=tr("模式：");
    switch(gameType)
    {
        case PERSON:
            str+=tr("双人模式");
        break;
        case BOT:
            str+=tr("人机模式");
        break;
        case NETWORK:
            str+=tr("网络模式");
        break;
    }
    str+=" ";
    str+=tr(" 状态：");
    if(model->currentIsBlack)str+="正等待黑方下棋";
    else str+="正等待白方下棋";
    str+= " ";
    if(model->playerFlag)str+="（我方下棋）";
    else str+= "(我方等待)";
   // statusTip(str);
    this->setStatusTip(str);
    this->statusBar()->showMessage(str);
}
void MainWindow::initPVPGame()
{
    gameType = PERSON;
    model->gameStatus = PLAYING;
    model->startGame(gameType);
    update();
}

void MainWindow::initPVEGame()
{
    gameType = BOT;
    model->gameStatus = PLAYING;
    model->startGame(gameType);
    if (QMessageBox::Yes == QMessageBox::question(this,
                                                  tr("Question"),
                                                  tr("机器先手吗?"),
                                                  QMessageBox::Yes | QMessageBox::No,
                                                  QMessageBox::Yes)) {
        //QMessageBox::information(this, tr("Hmmm..."), tr("I'm glad to hear that!"));
        model->playerFlag=false;
        this->chessOneByAI();
    } else {
        //QMessageBox::information(this, tr("Hmmm..."), tr("I'm sorry!"));
    }
    update();
     updateStatusTips();
}
MainWindow::~MainWindow()
{
    delete ui;
    delete model;
}
void MainWindow::mouseReleaseEvent(QMouseEvent* event)
{
    if(!model->testFlag)
    {
    // 人下棋，并且不能抢机器的棋
        if (model->playerFlag)
        {
            chessOneByPerson();

            // 如果是人机模式，需要调用AI下棋
            if (model->gameType == BOT && !model->playerFlag)
            {
                qDebug()<<model->playerFlag<<" "<<model->currentIsBlack;
                // 用定时器做一个延迟
                QTimer::singleShot(AIDELAY, this, &MainWindow::chessOneByAI);
            }
        }
    }
}
void MainWindow::chessOneByPerson()
{
    // 根据当前存储的坐标下子
    // 只有有效点击才下子，并且该处没有子
    if (clickPosRow != -1 && clickPosCol != -1 && model->gameMap[clickPosRow][clickPosCol] == 0)
    {
        model->actionByPerson(clickPosRow, clickPosCol);
       // QSound::play(CHESS_ONE_SOUND);

        // 重绘
        update();

    }
    updateStatusTips();
}
/*
void MainWindow::chessOneByStorage()
{
    if (clickPosRow >0 && clickPosCol >0 && model->gameMap[clickPosRow][clickPosCol] == 0)
    {
        model->actionByPerson(clickPosRow, clickPosCol);
       // QSound::play(CHESS_ONE_SOUND);

        // 重绘
    }
    update();
}*/
void MainWindow::chessOneByAI()
{
    if(backFlag!=0x10)
    {model->actionByAI2(clickPosRow, clickPosCol);
    //QSound::play(CHESS_ONE_SOUND);
        update();
    }else
    {
        backFlag = 0;
    }
     updateStatusTips();
}

void MainWindow::chessOneByHelper()
{
    if (!(gameType == BOT && !model->playerFlag))
    {
        chessOneByAI();
        // 如果是人机模式，需要调用AI下棋
        if (model->gameType == BOT && !model->playerFlag)
        {
            // 用定时器做一个延迟
           QTimer::singleShot(AIDELAY, this, &MainWindow::chessOneByAI);
        }
    }
     updateStatusTips();
}
void MainWindow::chessBack()
{
    int row,col;
    if (QMessageBox::Yes == QMessageBox::question(this,
                                                  tr("Question"),
                                                  tr("您确定要悔棋吗"),
                                                  QMessageBox::Yes | QMessageBox::No,
                                                  QMessageBox::Yes)) {
        //QMessageBox::information(this, tr("Hmmm..."), tr("I'm glad to hear that!"));
        model->setEmptyPosition(row,col);
        if(!model->playerFlag)model->setEmptyPosition(row,col);
        else if(model->gameType==BOT)backFlag = 0x10;
    }
     updateStatusTips();
}
void MainWindow::LightOneByHelper()
{
    model->testFlag = true;
    model->actionByAI2(clickPosRow, clickPosCol);
    sRow = clickPosRow;
    sCol = clickPosCol;
    sColor = model->gameMap[sRow][sCol];
    deep = 0;
    QTimer::singleShot(AIDELAY, this, &MainWindow::light);
    model->gameMap[sRow][sCol]=0;
    update();
    //model->testFlag = false;
}
void MainWindow::light()
{
    if(model->gameMap[sRow][sCol]!=0)
    {
        model->gameMap[sRow][sCol]=0;
        update();
        deep++;
       if(deep<6)QTimer::singleShot(AIDELAY, this, &MainWindow::light);
       else
       {
           model->testFlag = false;
       }
    }else
    {
        model->gameMap[sRow][sCol]=sColor;
        update();
        deep++;
        QTimer::singleShot(AIDELAY, this, &MainWindow::light);
    }
}
